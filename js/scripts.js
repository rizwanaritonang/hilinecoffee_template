function equalHeight(group) {
	tallest = 0;
	$(window).on("load resize", function(){
		if ($(window).width() > 979  ) {
			group.each(function() {
				thisHeight = $(this).innerHeight();
				if(thisHeight > tallest) {
					tallest = thisHeight;
				}
			});
			group.innerHeight(tallest);
		}
	});
}

(function ($) {
	'use strict';
	// NAV-HEADER
	$('.nav-header ul').superfish({
		delay: 400,
		animation: {
			opacity: 'show',
			height: 'show'
		},
		animationOut: {
			opacity: 'hide',
			height: 'hide'
		},
		speed: 200,
		speedOut: 200,
		autoArrows: false
	});

	// HOMEPAGE-CAROUSEL
	// $('.homepage-carousel').owlCarousel({
	// 	animateOut: 'fadeOut',
	// 	items: 1,
	// 	smartSpeed: 100,
	// 	autoplay: false,
	// 	autoplayTimeout: 3000,
	// 	loop: true,
	// });

	// QUOTE-CAROUSEL
	// $('.quote-carousel').owlCarousel({
	// 	animateOut: 'fadeOut',
	// 	items: 1,
	// 	smartSpeed: 100,
	// 	autoplay: false,
	// 	autoplayTimeout: 2000,
	// 	loop: true,
	// });

	// CONFIG ISOTOPE
	// var $container = $(".ins-container");
	// $container.imagesLoaded( function() {
	// 	$container.isotope();
	// });

	// $(".ins-filter a").click( function() {
	// 	var selector = $(this).attr("data-filter");
	// 	$container.isotope({
	// 		itemSelector: ".item.ins",
	// 		filter: selector
	// 	});
	// 	return false;
	// });

	// $(".ins-filter a").click( function (e) {
	// 	$(".ins-filter a").removeClass("active");
	// 	$(this).addClass("active");
	// });

	// NAVIGATION HANDLER
	$(".nav-header > ul").clone(false).appendTo(".nav-rwd-sidebar");
	$(window).on('load', function(){
		$('.nav-rwd-sidebar').find('ul').removeClass();
	});

	$(".btn-rwd-sidebar, .btn-hide").click( function(e) {
		var sidebar_wrapper = $(".nav-rwd-sidebar");
		var main_wrapper = $(".wrapper-inner");
		
		sidebar_wrapper.toggleClass("sidebar-active");
		main_wrapper.toggleClass("wrapper-active");
	});

	 //DROPDOWN
    $("#ddNav").click(function () {
        $(".ddown-list ul").toggle();
    });
    $(".ddown-list ul li a").click(function () {
        var text = $(this).html();
        $("#ddNav span").html(text);
        $(".ddropdown ul").hide();
        //var source = $("#source");
        //source.val($(this).find("span.value").html())
    });

     $('.bxslider').bxSlider({
			mode: 'fade',
			speed: 1000,
			pager: false,
			controls: false,
			auto: true,
     		pause: 7000
     });

     $('.filterNav').on('click', function(){
     	//ADD and REmove icons
     	$('.filterNav').removeClass('selected');
     	$(this).toggleClass('selected');

     	//OPEN NEXT CONTENT
     	$(this).closest('.subs-boxItem').next().find('.ct-target').slideDown();

     	// fetch the class of the clicked item
		var ourClass = $(this).attr('rel');

		
		if(ourClass == 'all') {
			// show all our items
			//$('#filterHolder').children('div.filt-item').show();	
		}
		else {
			// hide all elements that don't share ourClass
			$('#filterHolder').children(':not(.' + ourClass + ')').hide();
			// show all elements that do share ourClass
			$('#filterHolder').children('.' + ourClass).show();
		}
		return false;
     });


     //TOGGLE SUBSCRIPTION
	var allPanels = $('.ct-target').hide();   
	$('.ct-target:first').show();   
   	$('.nextStep').click(function() {
   		 var $getParent = $(this).closest('.subs-boxItem');
	     allPanels.slideUp();
	     $getParent.find('.subs-boxhead').addClass('active');	
	     $getParent.next().find('.ct-target').slideDown();
	     return false;
   });

   $('.subs-boxhead').on('click', function(){
   		if($('.subs-boxhead').is('.active')){
   			$(this).next().slideDown();
   		}
   });

   //EQUAL HEIGHT
   equalHeight($(".eqHeight"));
   
   //SHOW OFFER BOX
   $('.offer-box').delay(8000).addClass('show');
   $('.offer-box__close').on('click', function(){
   		$('.offer-box').removeClass('show');
   		return false;
   });

   $('.acc-row__edit').on('click', function(){
   		var $parentTarget = $(this).closest('.acc-row');
   		$parentTarget.find('.cc-row__form').slideToggle('slow', function(){
   			if($(this).is(':visible')){
   				$(this).closest('.acc-row').find('.acc-row__edit').html('<i class="fa fa-times"></i>');
   			}else{
   				$(this).closest('.acc-row').find('.acc-row__edit').html('<span>Edit</span>');
   			}
   		});
   		return false;
   });

   $('.showAddForm').on('click', function(){
   		var $parentTarget = $(this).parent().parent();
   		$parentTarget.find('.acc_addForm').slideToggle('slow', function(){
   			if($(this).is(':visible')){
   				$(this).closest('.acc-row').find('.acc-row__edit').html('<i class="fa fa-times"></i>');
   			}else{
   				$(this).closest('.acc-row').find('.acc-row__edit').html('<span>Edit</span>');
   			}
   		});
   		return false;
   });

   //Tooltips
	$(".tip_trigger").hover(function(){
		var tips = $(this).find('.tip');
		tips.show(); //Show tooltip
	}, function() {
		tips.hide(); //Hide tooltip		  
	}).mousemove(function(e) {
		var mousex = e.pageX + 20; //Get X coodrinates
		var mousey = e.pageY + 20; //Get Y coordinates
		var tipWidth = tips.width(); //Find width of tooltip
		var tipHeight = tips.height(); //Find height of tooltip
		
		//Distance of element from the right edge of viewport
		var tipVisX = $(window).width() - (mousex + tipWidth);
		//Distance of element from the bottom of viewport
		var tipVisY = $(window).height() - (mousey + tipHeight);
		  
		if ( tipVisX < 20 ) { //If tooltip exceeds the X coordinate of viewport
			mousex = e.pageX - tipWidth - 20;
		} if ( tipVisY < 20 ) { //If tooltip exceeds the Y coordinate of viewport
			mousey = e.pageY - tipHeight - 20;
		} 
		tip.css({  top: mousey, left: mousex });
	});


	$('.navFade_Form').on('click', function(){
		var $target = $(this).closest('.content-acc');
		event.preventDefault();
		$target.find('.fadeBox').fadeOut(100, function(){
			$target.find('.fade_form').fadeIn(600);
		});

		if($target.find('.fade_form').is(':visible')){
			$target.find('.fade_form').fadeOut(100);
			$target.find('.fadeBox').fadeIn(600);
			$('.navFade_Form').html('<span>Add New</span>');
		}else{
			$('.navFade_Form').html('<i class="fa fa-times"></i>');
		}
	});

	$(".hideSiblings:first-child").show();
	$('.arr-tbToggle').on('click', function(){
		event.preventDefault();
		$(this).find('span').toggleClass("fa fa-caret-down");
		$(this).closest('.hideSibling').siblings().slideToggle();

	});

	//POPUP
	$(".fancybox").fancybox();

	$('.togNav-head').on('click', function(){
		$(this).find('.fa').toggleClass("fa fa-caret-right fa fa-caret-down");
		$(this).closest('.faqmain-wrap').find('.faqmain-desc').slideToggle('fast');
	});

	$(".video-wrapper").fitVids();
	
	//ADD INTIAL CLASS TO HIDE ON SELECT
	$(".nav-left ul ul").children().addClass('list-dd');
	//NAV OT SELECT MENU
    $("<select />").appendTo(".nav-left");
    $("<option />", {
       "selected": "selected",
       "value"   : "",
       "text"    : "-- Select Menu --"
    }).appendTo(".nav-left select");
    $(".nav-left ul > li a").each(function() {
     var el = $(this);
     $("<option />", {
     	 "class"   : el.parent().attr('class'),
         "value"   : el.attr("href"),
         "text"    : el.text()
     }).appendTo(".nav-left select");
    });
    $(".nav-left select").change(function() {
      window.location = $(this).find("option:selected").val();
    });
    $(".nav-left select").find('option.list-dd').hide();

})(jQuery);